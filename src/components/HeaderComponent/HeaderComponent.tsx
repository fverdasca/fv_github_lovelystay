import * as React from 'react';
import './HeaderComponent.scss';
import {
    NavLink,
} from 'react-router-dom';
import { IRoutes } from '../../App';
import logo from '../../assets/lovely_logo.png';

export interface HeaderComponentProps {
    routes: IRoutes[];
}
 
const HeaderComponent: React.FC<HeaderComponentProps> = ({ routes }) => {
    return (
        <div className="header-component bg-light">
            <img src={logo} alt="lovely logo"/>
            <nav>
                <ul>
                    {routes.map( (_route, _index) => _route.displayOnMenu && (
                        <li key={_index}>
                            <NavLink exact to={_route.path} activeClassName="active">
                                <span className="btn-primary text-primary-xs-300">{_route.title}</span>
                            </NavLink>
                        </li>
                    ))}
                </ul> 
            </nav>
        </div>
    );
}
 
export default HeaderComponent;
import * as React from 'react';
import './SearchComponent.scss';

export interface IInput {
    id: string;
    message: string | null;
}

export interface IBtn {
    title: string;
    fn: Function;
}

export interface SearchComponentProps {
    input: IInput;
    btn: IBtn;
}

const SearchComponent: React.FC<SearchComponentProps> = ({input, btn}) => {

    //state & fn to track inputs
    const [inputText, setInputText] = React.useState<string>('');

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputText(e.currentTarget.value);
    }

    const handleSearchClick = () => {
        btn.fn(inputText);
        setInputText("");
    }

    return (
        <div className="input">
            <input onChange={handleInputChange} className="primary-text-input text-gray-xs-300" type="text" name="username search" data-testid="search-input" id={input.id}/>
            {input.message && <span data-testid="input-message" className="text-secondary-xs-400">{input.message}</span>}
            <button onClick={handleSearchClick} className="btn-primary-outlined text-primary-xs-400" data-testid="search-btn">{btn.title}</button>
        </div>
    );
}
 
export default SearchComponent;
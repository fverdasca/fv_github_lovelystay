import React from "react";
import { render, fireEvent } from "@testing-library/react";
import SearchComponent from "./SearchComponent";

//created mocked object to populate props for the component
const props = {
    input: {
        title: "test-id",
        message: null,
    },
    btn: {
        title: "test-button",
        fn: jest.fn(),
    },
};

it("renders correctly", () => {
    const { queryByTestId } = render(<SearchComponent {...props} />);

    //confirm both elements were successfully inserted in HTML
    expect(queryByTestId("search-btn")).toBeTruthy();
    expect(queryByTestId("search-input")).toBeTruthy();
    //confirm message received as null
    expect(queryByTestId("input-message")).not.toBeTruthy();
});

describe("input value", () => {
    it("updates on change", () => {
        const { queryByTestId } = render(<SearchComponent {...props} />);
        const searchInput = queryByTestId("search-input");

        //confirm default input value at search
        expect(searchInput.value).toBe("");

        //change input value
        fireEvent.change(searchInput, {
            target: { value: "testing" },
        });
        //confirm new input value at search
        expect(searchInput.value).toBe("testing");
    });
});

describe("search button", () => {
    it("renders the prop title", () => {
        const { queryByTestId } = render(<SearchComponent {...props} />);
        const searchBtn = queryByTestId("search-btn");

        //confirm button title equals to the same received in props
        expect(searchBtn).toHaveTextContent("test-button");
    });
    it("triggers a function", () => {
        const { queryByTestId } = render(<SearchComponent {...props} />);
        const searchBtn = queryByTestId("search-btn");
        const searchInput = queryByTestId("search-input");

        //click search button
        fireEvent.click(searchBtn);
        //confirm prop function is called
        expect(props.btn.fn).toHaveBeenCalled();
        //confirm search input returns to empty string
        expect(searchInput.value).toBe("");
    });
});

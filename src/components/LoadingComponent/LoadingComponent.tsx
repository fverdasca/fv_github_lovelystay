import * as React from 'react';
import "./LoadingComponent.scss";
import logo from '../../assets/lovely_logo.png';

export interface LoadingComponentProps {
    location?: any
}
 
const LoadingComponent: React.FC<LoadingComponentProps> = () => {

    return (
        <div>
            <div className="loading-container">
                <img src={logo} alt="lovely logo"/>
                <p className="text-gray-xs-300">Loading...</p>
            </div>
        </div>
    );
}
 
export default LoadingComponent;
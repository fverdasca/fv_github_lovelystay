import * as React from 'react';
import './IntroComponent.scss';

export interface IntroComponentProps {
    title: string;
    description: string;
}
 
const IntroComponent: React.FC<IntroComponentProps> = ({title, description}) => {

    return (
        <section className="page-info">
            <h2 className="text-primary-l-300">{title}</h2>    
            <p className="text-gray-xs-300">{description}</p>
        </section>
    );
}
 
export default IntroComponent;
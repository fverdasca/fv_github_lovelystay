import "./App.scss";
import * as React from 'react';
import {
    BrowserRouter,
    Route,
    Switch,
} from 'react-router-dom';
import HeaderComponent from "./components/HeaderComponent/HeaderComponent";
import Home from "./pages/Home/Home";
import Search from "./pages/Search/Search";
import User from "./pages/User/User";

export interface IRoutes {
    path: string;
    title: string;
    component: any;
    displayOnMenu: boolean;
}

function App() {

    const routes: IRoutes[] = [
        {
            path: "/",
            title: "Home",
            component: <Home />,
            displayOnMenu: true
        },
        {
            path: "/search",
            title: "Search",
            component: <Search />,
            displayOnMenu: true
        },
        {
            path: "/user/:username",
            title: "User",
            component: <User />,
            displayOnMenu: false
        }
    ]

    return (
        <div className="start-app">
            <BrowserRouter>
                <header className="app-header">
                    <HeaderComponent routes={routes} />
                </header>
                <main className="app-main container">
                    <Switch>
                            {routes.map( (_route, _index) => (
                                <Route key={_index} exact path={_route.path}>
                                    {_route.component}
                                </Route>
                            ))}
                    </Switch>
                </main>
            </BrowserRouter>
        </div>
    );
}

export default App;

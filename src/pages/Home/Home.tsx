import * as React from 'react';
import './Home.scss';
import IntroComponent from '../../components/IntroComponent/IntroComponent';

export interface HomeProps {
    
}
 
const Home: React.FC<HomeProps> = () => {

    const title: string = "Home";
    const description: string = "This is the homepage for the Github API project";

    return (
        <div>
            <IntroComponent title={title} description={description} />
            <ol className="info-list">
                <li className="text-gray-xs-400">if typescript version from VSCode is different from typescript installed - make sure to run the ts installed at libs (to solve react-jsx problem at tsconfig)</li>
                <li className="text-gray-xs-300">_variables.scss and _config.scss are the files for sass configs</li>
                <li className="text-gray-xs-400">for more routes, add them to the routes object at App.tsx</li>
                <li className="text-gray-xs-300">each route has a React component inside pages folder</li>
                <li className="text-gray-xs-300">pages are consuming components from the components folder</li>
                <li className="text-gray-xs-400">user is searched at Search route and sent by URL params to be fetched at User route</li>
                <li className="text-gray-xs-300">unit tests added to the text input and search button</li>
            </ol>
        </div>
    );
}
 
export default Home;
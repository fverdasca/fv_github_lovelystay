import * as React from 'react';
import "./User.scss";
import { useHistory, useParams } from "react-router-dom";
import IntroComponent, { IntroComponentProps } from '../../components/IntroComponent/IntroComponent';
import LoadingComponent from '../../components/LoadingComponent/LoadingComponent';
import Axios from 'axios';
import NotFound from '../../assets/undraw_people_search_wctu.svg';

interface IUser {
    name: string;
    picture: string;
    repos: any[];
}

interface IParams {
    username: string;
}
 
const User: React.FC = () => {

    //define history to push search & params to use url params
    const history = useHistory();
    const params: IParams = useParams();

    //use effect hook to make the fetch on component mount
    React.useEffect(() => {
        requestUser(params.username);
    }, []);

    //requests and data manipulation
    //first request to get the user
    const usersUrl: string = "https://api.github.com/users/";
    const [user, setUser] = React.useState<IUser | null>(null);
    const [loading, setLoading] = React.useState<boolean>(false);
    const [error, setError] = React.useState<string | null>(null);

    const requestUser = (name: string) => {
        setLoading(true);
        Axios.get(usersUrl + name)
            .then((response) => {
                const userData = response.data;
                if (userData.public_repos > 0) {
                    requestRepos(userData, name)
                } else {
                    handleData(userData);
                }
            })
            .catch((error) => {
                setError(error.message);
                setLoading(false);
            })
    }

    //second request to get the repos (if there are repos)
    const requestRepos = (userData: any, name: string) => {
        Axios.get(usersUrl + name + "/repos")
            .then((response) => {
                const userRepos = response.data;
                handleData(userData, userRepos);
            })
            .catch((error) => {
                setError(error.message);
                setLoading(false);
            })
    }

    //function to populate user
    const handleData = (userData: any, userRepos?: any[]) => {
        let reposArray: any = [];
        if (userRepos) reposArray = [...userRepos];
        setLoading(false);
        setUser({
            name: userData.name,
            picture: userData.avatar_url,
            repos: reposArray,
        })
    }

    //variables to populate IntroComponent
    const propsToIntroComponent: IntroComponentProps = {
        title: "User",
        description: "Check profile of the user found"
    }

    const userHtml = () => {
        return (
            <>
                <div className="card user shadow-s">
                    <img src={user!.picture} alt="profile"/>
                    <h3 className={!user!.name ? 'text-secondary-m-300' : 'text-primary-m-400'}>{user!.name || 'Missing name'}</h3>
                </div>
                {
                    user!.repos.length > 0 ? (
                        <div className="card repo shadow-s">
                            {user!.repos?.map((_repo, _index) => (
                                <div key={_index} className="repos">
                                    <h4 className={!_repo.name ? 'text-secondary-xs-300' : 'text-primary-s-400'}>{_repo.name || 'Missing title'}</h4>
                                    <p className={!_repo.description ? 'text-secondary-xs-300' : 'text-gray-xs-300'}>{_repo.description || 'Missing description'}</p>
                                </div>
                            ))}
                        </div>
                    ) : (
                        <p className="text-secondary-xs-300 no-repo-fallback">This user has no public repositories!</p>
                    )
                }
            </>
        )
    }

    const errorFallback = () => {
        return (
            <div className="error-fallback">
                <img src={NotFound} alt="fallback error"/>
                <p className="text-gray-xs-300">{error}</p>
            </div>
        ) 
    }

    return (
        <div>
            <IntroComponent {...propsToIntroComponent} />
            <div className="user-page-container">
            { loading ? <LoadingComponent /> : (
                <>
                    <button onClick={ () => history.goBack()} className="btn-primary-outlined text-primary-xs-400">Go back</button>
                    <section className="user-info">
                        { user ? userHtml() : errorFallback() }
                    </section>
                </>
            )}
            </div>
        </div>
    );
}
 
export default User;
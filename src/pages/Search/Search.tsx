import * as React from 'react';
import './Search.scss';
import { Redirect } from 'react-router-dom';
import IntroComponent from '../../components/IntroComponent/IntroComponent';
import SearchComponent from '../../components/SearchComponent/SearchComponent';
 
const Search: React.FC = () => {

    //variables to populate PageInfoComponent
    const title: string = "Search";
    const description: string = "Here you can search for a user in Github";

    //state & fn to track inputs
    const [invalidMessage, setInvalidMessage] = React.useState<string | null>(null);
    const [userName, setUserName] = React.useState<string | null>(null);

    const handleSearch = (text: string) => {
        if (text !== '') {
            setUserName(text);
            setInvalidMessage(null);
        }
        else setInvalidMessage('Required');
    }

    return (
        <div>
            <IntroComponent title={title} description={description} />
            <div className="page-content">
                <div className="input-container">
                    <header>
                        <h3 className="text-primary-s-400">Github api</h3>
                        <p className="text-gray-xs-300">Type a github username and click at search button</p>
                    </header>
                    <SearchComponent 
                        input={{
                            id: "username-search",
                            message: invalidMessage
                        }}
                        btn={{
                            title: 'Search',
                            fn: handleSearch
                        }}
                    />
                </div>
            </div>
            { userName && <Redirect push to={"/user/" + userName} /> }
        </div>
    );
}
 
export default Search;
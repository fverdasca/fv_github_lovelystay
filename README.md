# Project setup

* Typescript: change **jsx** option to "react" at **tsconfig.json** after running npm start - it is changing it to "react-jsx" and I didn't find a proper way to solve it yet
* Sass: **_config** is the main sass generator for all project usage - is consuming variables from the **_variables** file

***

# Project architecture

* Pages folder: each page/route is inside this folder - Home, Search, User
* Components folder: not only the reusable components were added here - other components were added to have a cleaner and better understandable code

***

# Unit testing

* I've created tests for the input & button elements - **SearchComponent**